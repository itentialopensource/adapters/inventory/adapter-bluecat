# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Bluecat System. The API that was used to build the adapter for Bluecat is usually available in the report directory of this adapter. The adapter utilizes the Bluecat API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The BlueCat adapter from Itential is used to integrate the Itential Automation Platform (IAP) with BlueCat. With this adapter you have the ability to perform operations such as:

- Performs IP management, DNS management, and data center management.
- Create DHCP Range
- Create DNS Record
- Create Subnet
- Create IP Record
- Get Next Available IP

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
