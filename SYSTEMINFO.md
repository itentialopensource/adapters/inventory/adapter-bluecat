# BlueCat

Vendor: BlueCat
Homepage: https://bluecatnetworks.com/

Product: BlueCat
Product Page: https://bluecatnetworks.com/

## Introduction
We classify BlueCat into the Inventory domain as BlueCat manages and provides information about network resources and configurations.

"BlueCat provides flexible, secure, and automated network solutions that make complex network change feel trivial"

## Why Integrate
The BlueCat adapter from Itential is used to integrate the Itential Automation Platform (IAP) with BlueCat. With this adapter you have the ability to perform operations such as:

- Performs IP management, DNS management, and data center management.
- Create DHCP Range
- Create DNS Record
- Create Subnet
- Create IP Record
- Get Next Available IP

## Additional Product Documentation
The [API documents for BlueCat](https://docs.bluecatnetworks.com/r/Address-Manager-API-Guide/API-Object-Method-Reference/9.3.0)