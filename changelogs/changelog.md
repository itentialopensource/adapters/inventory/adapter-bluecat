
## 0.3.0 [09-22-2022]

* Minor/adapt 2388

See merge request itentialopensource/adapters/inventory/adapter-bluecat!2

---

## 0.2.0 [05-22-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/inventory/adapter-bluecat!1

---

## 0.1.3 [06-17-2021] & 0.1.2 [06-17-2021] & 0.1.1 [06-14-2021]

- Initial Commit

See commit 6e09aed

---
