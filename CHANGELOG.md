
## 0.7.4 [10-15-2024]

* Changes made at 2024.10.14_20:31PM

See merge request itentialopensource/adapters/adapter-bluecat!17

---

## 0.7.3 [08-24-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-bluecat!15

---

## 0.7.2 [08-14-2024]

* Changes made at 2024.08.14_18:44PM

See merge request itentialopensource/adapters/adapter-bluecat!14

---

## 0.7.1 [08-07-2024]

* Changes made at 2024.08.06_19:57PM

See merge request itentialopensource/adapters/adapter-bluecat!13

---

## 0.7.0 [08-05-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/inventory/adapter-bluecat!12

---

## 0.6.7 [03-26-2024]

* Changes made at 2024.03.26_14:45PM

See merge request itentialopensource/adapters/inventory/adapter-bluecat!11

---

## 0.6.6 [03-21-2024]

* Changes made at 2024.03.21_14:40PM

See merge request itentialopensource/adapters/inventory/adapter-bluecat!10

---

## 0.6.5 [03-13-2024]

* Changes made at 2024.03.13_14:03PM

See merge request itentialopensource/adapters/inventory/adapter-bluecat!9

---

## 0.6.4 [03-11-2024]

* Changes made at 2024.03.11_14:12PM

See merge request itentialopensource/adapters/inventory/adapter-bluecat!8

---

## 0.6.3 [02-26-2024]

* Changes made at 2024.02.26_13:40PM

See merge request itentialopensource/adapters/inventory/adapter-bluecat!7

---

## 0.6.2 [01-26-2024]

* update axios and metadata

See merge request itentialopensource/adapters/inventory/adapter-bluecat!6

---

## 0.6.1 [12-24-2023]

* update axios and metadata

See merge request itentialopensource/adapters/inventory/adapter-bluecat!6

---

## 0.6.0 [12-14-2023]

* More migration changes

See merge request itentialopensource/adapters/inventory/adapter-bluecat!5

---

## 0.5.0 [11-07-2023]

* More migration changes

See merge request itentialopensource/adapters/inventory/adapter-bluecat!5

---

## 0.4.2 [09-27-2023]

* more metadata changes

See merge request itentialopensource/adapters/inventory/adapter-bluecat!4

---

## 0.4.1 [09-27-2023]

* more metadata changes

See merge request itentialopensource/adapters/inventory/adapter-bluecat!4

---

## 0.4.0 [09-26-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/inventory/adapter-bluecat!3

---

## 0.3.0 [09-22-2022]

* Minor/adapt 2388

See merge request itentialopensource/adapters/inventory/adapter-bluecat!2

---

## 0.2.0 [05-22-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/inventory/adapter-bluecat!1

---

## 0.1.3 [06-17-2021] & 0.1.2 [06-17-2021] & 0.1.1 [06-14-2021]

- Initial Commit

See commit 6e09aed

---
