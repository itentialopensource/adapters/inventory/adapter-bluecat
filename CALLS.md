## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for BlueCat. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for BlueCat.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the BlueCat. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">addACL(configurationId, name, properties, callback)</td>
    <td style="padding:15px">Adds an Access Control List (ACL) to a view.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addACL?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAliasRecord(absoluteName, linkedRecordName, properties, ttl, viewId, callback)</td>
    <td style="padding:15px">Adds alias records.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addAliasRecord?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addBulkHostRecord(absoluteName, networkId, numberOfAddresses, properties, startAddress, ttl, viewId, callback)</td>
    <td style="padding:15px">Adds bulk host records using auto-increment from the specific starting address.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addBulkHostRecord?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDNSDeploymentOption(entityId, name, properties, value, callback)</td>
    <td style="padding:15px">Adds DNS options.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addDNSDeploymentOption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addEnumNumber(enumZoneId, number, properties, callback)</td>
    <td style="padding:15px">Adds ENUM numbers.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addEnumNumber?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addEnumZone(parentId, prefix, properties, callback)</td>
    <td style="padding:15px">Adds ENUM zones.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addEnumZone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addExternalHostRecord(name, properties, viewId, callback)</td>
    <td style="padding:15px">Adds external host records.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addExternalHostRecord?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addGenericRecord(absoluteName, properties, rdata, ttl, type, viewId, callback)</td>
    <td style="padding:15px">Adds generic records.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addGenericRecord?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addHINFORecord(absoluteName, cpu, os, properties, ttl, viewId, callback)</td>
    <td style="padding:15px">Adds HINFO records.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addHINFORecord?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addHostRecord(absoluteName, addresses, properties, ttl, viewId, callback)</td>
    <td style="padding:15px">Adds host records for IPv4 or IPv6 addresses.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addHostRecord?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addMXRecord(absoluteName, linkedRecordName, priority, properties, ttl, viewId, callback)</td>
    <td style="padding:15px">Adds MX records.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addMXRecord?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addNAPTRRecord(absoluteName, flags, order, preference, properties, regexp, replacement, service, ttl, viewId, callback)</td>
    <td style="padding:15px">Adds NAPTR records.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addNAPTRRecord?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addResourceRecord(absoluteName, properties, rdata, ttl, type, viewId, callback)</td>
    <td style="padding:15px">Add resource records.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addResourceRecord?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addResponsePolicy(configurationId, name, properties, responsePolicyType, ttl, callback)</td>
    <td style="padding:15px">Adds a DNS response policy.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addResponsePolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addResponsePolicyItem(itemName, options, policyId, callback)</td>
    <td style="padding:15px">Adds a DNS response policy item under a specified local DNS response policy.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addResponsePolicyItem?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSRVRecord(absoluteName, linkedRecordName, port, priority, properties, ttl, viewId, weight, callback)</td>
    <td style="padding:15px">Adds SRV records.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addSRVRecord?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addStartOfAuthority(email, expire, minimum, parentId, properties, refresh, retry, callback)</td>
    <td style="padding:15px">Adds SOA records.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addStartOfAuthority?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTXTRecord(absoluteName, properties, ttl, txt, viewId, callback)</td>
    <td style="padding:15px">Adds TXT records.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addTXTRecord?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addView(configurationId, name, properties, callback)</td>
    <td style="padding:15px">Adds DNS views.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addView?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addZone(absoluteName, parentId, properties, callback)</td>
    <td style="padding:15px">Adds DNS zones.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addZone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addZoneTemplate(name, parentId, properties, callback)</td>
    <td style="padding:15px">Adds a DNS zone template.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addZoneTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSDeploymentOption(entityId, name, serverId, callback)</td>
    <td style="padding:15px">Deletes DNS options.</td>
    <td style="padding:15px">{base_path}/{version}/v1/deleteDNSDeploymentOption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteResponsePolicyItem(itemName, options, policyId, callback)</td>
    <td style="padding:15px">Deletes a DNS response policy item under a specified local DNS response
policy.</td>
    <td style="padding:15px">{base_path}/{version}/v1/deleteResponsePolicyItem?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findResponsePoliciesWithItem(configurationId, itemName, options, callback)</td>
    <td style="padding:15px">Finds local DNS response policies with their associated response policy items.</td>
    <td style="padding:15px">{base_path}/{version}/v1/findResponsePoliciesWithItem?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSDeploymentOption(entityId, name, serverId, callback)</td>
    <td style="padding:15px">Retrieves all DNS options assigned for the object specified excluding the
options inherited from th</td>
    <td style="padding:15px">{base_path}/{version}/v1/getDNSDeploymentOption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostRecordsByHint(count, options, start, callback)</td>
    <td style="padding:15px">Returns an array of objects with host record type.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getHostRecordsByHint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getKSK(entityId, format, callback)</td>
    <td style="padding:15px">Returns a string containing all active Key Signing Keys (KSK) for a given
entityId value in a speci</td>
    <td style="padding:15px">{base_path}/{version}/v1/getKSK?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkLinkedProperties(networkId, callback)</td>
    <td style="padding:15px">Returns an array of IP addresses with linked records and the IP addresses
that are assigned as DHCP</td>
    <td style="padding:15px">{base_path}/{version}/v1/getNetworkLinkedProperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZonesByHint(containerId, count, options, start, callback)</td>
    <td style="padding:15px">Returns an array of accessible zones of child objects for a given containerId
value.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getZonesByHint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">moveResourceRecord(destinationZone, resourceRecordId, callback)</td>
    <td style="padding:15px">Moves resource records between different zones that already exist.</td>
    <td style="padding:15px">{base_path}/{version}/v1/moveResourceRecord?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchResponsePolicyItems(count, keyword, properties, scope, start, callback)</td>
    <td style="padding:15px">Searches Response Policy items configured in local Response Policies or
predefined BlueCat Security</td>
    <td style="padding:15px">{base_path}/{version}/v1/searchResponsePolicyItems?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDNSDeploymentOption(body, callback)</td>
    <td style="padding:15px">Updates DNS options.</td>
    <td style="padding:15px">{base_path}/{version}/v1/updateDNSDeploymentOption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadResponsePolicyItems(inputStream, parentId, body, callback)</td>
    <td style="padding:15px">Uploads one response policy file containing a list of fully qualified domain
names (FQDNs).</td>
    <td style="padding:15px">{base_path}/{version}/v1/uploadResponsePolicyItems?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAccessRight(entityId, overrides, properties, userId, value, callback)</td>
    <td style="padding:15px">Adds access rights to a specified object.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addAccessRight?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDevice(configurationId, deviceSubtypeId, deviceTypeId, ip4Addresses, ip6Addresses, name, properties, callback)</td>
    <td style="padding:15px">Adds a device to a configuration.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addDevice?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDeviceSubtype(name, parentId, properties, callback)</td>
    <td style="padding:15px">Adds a device sub-type to Address Manager.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addDeviceSubtype?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDeviceType(name, properties, callback)</td>
    <td style="padding:15px">Adds a device type to Address Manager.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addDeviceType?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addMACAddress(configurationId, macAddress, properties, callback)</td>
    <td style="padding:15px">Adds MAC addresses.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addMACAddress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTag(name, parentId, properties, callback)</td>
    <td style="padding:15px">Adds object tags.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addTag?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTagGroup(name, properties, callback)</td>
    <td style="padding:15px">Adds object tag groups.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addTagGroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addUser(password, properties, username, callback)</td>
    <td style="padding:15px">Adds Address Manager users.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addUserGroup(name, properties, callback)</td>
    <td style="padding:15px">Adds user groups.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addUserGroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateMACAddressWithPool(configurationId, macAddress, poolId, callback)</td>
    <td style="padding:15px">Associates a MAC address with a MAC pool.</td>
    <td style="padding:15px">{base_path}/{version}/v1/associateMACAddressWithPool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">breakReplication(callback)</td>
    <td style="padding:15px">Breaks replication and returns each server to its original stand-alone state.</td>
    <td style="padding:15px">{base_path}/{version}/v1/breakReplication?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configureStreamingReplication(compressReplication, latencyCriticalThreshold, latencyWarningThreshold, properties, standbyServer, callback)</td>
    <td style="padding:15px">Enables database replication on a remote system in order to automate the
setup of replication betwe</td>
    <td style="padding:15px">{base_path}/{version}/v1/configureStreamingReplication?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAccessRight(entityId, userId, callback)</td>
    <td style="padding:15px">Deletes an access right for a specified object.</td>
    <td style="padding:15px">{base_path}/{version}/v1/deleteAccessRight?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">denyMACAddress(configurationId, macAddress, callback)</td>
    <td style="padding:15px">Denies MAC addresses.</td>
    <td style="padding:15px">{base_path}/{version}/v1/denyMACAddress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">establishTrustRelationship(password, properties, remoteIP, username, callback)</td>
    <td style="padding:15px">Establishes a trust relationship between a maximum of three Address Manager
servers, which is a pre</td>
    <td style="padding:15px">{base_path}/{version}/v1/establishTrustRelationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">failoverReplication(properties, standbyServer, callback)</td>
    <td style="padding:15px">Performs a manual replication failover.</td>
    <td style="padding:15px">{base_path}/{version}/v1/failoverReplication?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccessRight(entityId, userId, callback)</td>
    <td style="padding:15px">Retrieves an access right for a specified object.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getAccessRight?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccessRightsForEntity(count, entityId, start, callback)</td>
    <td style="padding:15px">Returns an array of access rights for entities.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getAccessRightsForEntity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccessRightsForUser(count, start, userId, callback)</td>
    <td style="padding:15px">Returns an array of access rights for a specified user.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getAccessRightsForUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllUsedLocations(callback)</td>
    <td style="padding:15px">Returns a list of location objects that are used to annotate
other objects.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getAllUsedLocations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigurationGroups(callback)</td>
    <td style="padding:15px">Gets a list of all configuration groups in Address Manager.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getConfigurationGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigurationSetting(configurationId, settingName, callback)</td>
    <td style="padding:15px">Returns the configuration setting.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getConfigurationSetting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigurationsByGroup(groupName, properties, callback)</td>
    <td style="padding:15px">Gets a list of configurations in Address Manager based on the name of a
configuration group.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getConfigurationsByGroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLocationByCode(code, callback)</td>
    <td style="padding:15px">Returns the location object with the specified hierarchical
location code.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getLocationByCode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMACAddress(configurationId, macAddress, callback)</td>
    <td style="padding:15px">Returns an APIEntity for a MAC address.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getMACAddress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReplicationInfo(properties, callback)</td>
    <td style="padding:15px">Retrieves information regarding the status of replication in Address Manager
through the API.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getReplicationInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">purgeHistoryNow(numberOfDaysToKeep, numberOfMonthsToKeep, untilWhenTimestamp, waitOption, callback)</td>
    <td style="padding:15px">Runs the history purge function.</td>
    <td style="padding:15px">{base_path}/{version}/v1/purgeHistoryNow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeTrustRelationship(otherBAMIP, properties, callback)</td>
    <td style="padding:15px">Removes a remote Address Manager server from the trust relationship.</td>
    <td style="padding:15px">{base_path}/{version}/v1/removeTrustRelationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">terminateUserSessions(properties, username, callback)</td>
    <td style="padding:15px">Terminates all active user sessions in Address Manager.</td>
    <td style="padding:15px">{base_path}/{version}/v1/terminateUserSessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAccessRight(entityId, overrides, properties, userId, value, callback)</td>
    <td style="padding:15px">Updates access rights for a specified object.</td>
    <td style="padding:15px">{base_path}/{version}/v1/updateAccessRight?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateConfigurationSetting(configurationId, properties, settingName, callback)</td>
    <td style="padding:15px">Updates the configuration setting.</td>
    <td style="padding:15px">{base_path}/{version}/v1/updateConfigurationSetting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUserPassword(newPassword, options, userId, callback)</td>
    <td style="padding:15px">Updates an Address Manager user password.</td>
    <td style="padding:15px">{base_path}/{version}/v1/updateUserPassword?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAdditionalIPAddresses(ipsToAdd, properties, serverId, callback)</td>
    <td style="padding:15px">Adds additional IPv4 addresses and loopback addresses to the Services
interface for DNS service.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addAdditionalIPAddresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDeviceInstance(configName, deviceName, ipAddressMode, ipEntity, macAddressMode, macEntity, options, recordName, viewName, zoneName, callback)</td>
    <td style="padding:15px">Adds a device instance.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addDeviceInstance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIP4BlockByCIDR(cIDR, parentId, properties, callback)</td>
    <td style="padding:15px">Adds a new IPv4 Block using CIDR notation.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addIP4BlockByCIDR?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIP4BlockByRange(end, parentId, properties, start, callback)</td>
    <td style="padding:15px">Adds a new IPv4 block defined by an address range.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addIP4BlockByRange?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIP4IPGroupByRange(end, name, parentId, properties, start, callback)</td>
    <td style="padding:15px">Adds an IPv4 IP group by range bounds (start address and end address).</td>
    <td style="padding:15px">{base_path}/{version}/v1/addIP4IPGroupByRange?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIP4IPGroupBySize(name, parentId, positionRangeBy, positionValue, properties, size, callback)</td>
    <td style="padding:15px">Adds an IPv4 IP group by size.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addIP4IPGroupBySize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIP4Network(cIDR, blockId, properties, callback)</td>
    <td style="padding:15px">Adds an IPv4 network using CIDR notation.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addIP4Network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIP4NetworkTemplate(configurationId, name, properties, callback)</td>
    <td style="padding:15px">Add an IPv4 network template to the specified configuration.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addIP4NetworkTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIP4ReconciliationPolicy(name, parentId, properties, callback)</td>
    <td style="padding:15px">Adds an IPv4 reconciliation policy.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addIP4ReconciliationPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIP6Address(address, containerId, name, properties, type, callback)</td>
    <td style="padding:15px">Adds an IPv6 address to a specified IPv6 network.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addIP6Address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIP6BlockByMACAddress(macAddress, name, parentId, properties, callback)</td>
    <td style="padding:15px">Adds an IPv6 block by specifying the MAC address of the server.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addIP6BlockByMACAddress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIP6BlockByPrefix(name, parentId, prefix, properties, callback)</td>
    <td style="padding:15px">Adds an IPv6 block be specifying the prefix for the block.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addIP6BlockByPrefix?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIP6NetworkByPrefix(name, parentId, prefix, properties, callback)</td>
    <td style="padding:15px">Adds an IPv6 network be specifying the prefix for the network.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addIP6NetworkByPrefix?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addParentBlock(body, callback)</td>
    <td style="padding:15px">Adds a parent block.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addParentBlock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addParentBlockWithProperties(properties, body, callback)</td>
    <td style="padding:15px">Adds a parent block with properties.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addParentBlockWithProperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applyIP4NetworkTemplate(networkId, properties, templateId, callback)</td>
    <td style="padding:15px">Applies IPv4 network templates.</td>
    <td style="padding:15px">{base_path}/{version}/v1/applyIP4NetworkTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignIP4Address(action, configurationId, hostInfo, ip4Address, macAddress, properties, callback)</td>
    <td style="padding:15px">Assigns a MAC address and other properties to an IPv4 address.</td>
    <td style="padding:15px">{base_path}/{version}/v1/assignIP4Address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignIP4NetworkTemplate(networkId, properties, templateId, callback)</td>
    <td style="padding:15px">Assigns IPv4 network templates.</td>
    <td style="padding:15px">{base_path}/{version}/v1/assignIP4NetworkTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignIP6Address(action, address, containerId, hostInfo, macAddress, properties, callback)</td>
    <td style="padding:15px">Assigns an IPv6 address to a MAC address and host.</td>
    <td style="padding:15px">{base_path}/{version}/v1/assignIP6Address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignNextAvailableIP4Address(action, configurationId, hostInfo, macAddress, parentId, properties, callback)</td>
    <td style="padding:15px">Assign the next available IPv4 address.</td>
    <td style="padding:15px">{base_path}/{version}/v1/assignNextAvailableIP4Address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeStateIP4Address(addressId, macAddress, targetState, callback)</td>
    <td style="padding:15px">Converts the state of an address from and between Reserved, DHCP Reserved,
and Static, or DHCP Allo</td>
    <td style="padding:15px">{base_path}/{version}/v1/changeStateIP4Address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clearIP6Address(addressId, callback)</td>
    <td style="padding:15px">Clears a specified IPv6 address assignment.</td>
    <td style="padding:15px">{base_path}/{version}/v1/clearIP6Address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceInstance(configName, identifier, options, callback)</td>
    <td style="padding:15px">Deletes either the IP address or MAC address (and all related DNS entries
including host records, P</td>
    <td style="padding:15px">{base_path}/{version}/v1/deleteDeviceInstance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdditionalIPAddresses(adonisID, properties, callback)</td>
    <td style="padding:15px">Returns IPv4 addresses and loopback addresses added to the Service interface
for DNS services.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getAdditionalIPAddresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAliasesByHint(count, options, start, callback)</td>
    <td style="padding:15px">Returns an array of CNAMEs with linked record name.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getAliasesByHint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoveredDevice(deviceId, policyId, callback)</td>
    <td style="padding:15px">Returns the object ID of the discovered device by running an IPv4
reconciliation policy.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getDiscoveredDevice?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoveredDeviceArpEntries(deviceId, policyId, callback)</td>
    <td style="padding:15px">Returns all ARP entries of a specific device discovered by running an IPv4
reconciliation policy.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getDiscoveredDeviceArpEntries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoveredDeviceHosts(deviceId, policyId, callback)</td>
    <td style="padding:15px">Returns all hosts of a specific device discovered by running an IPv4
reconciliation policy.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getDiscoveredDeviceHosts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoveredDeviceInterfaces(deviceId, policyId, callback)</td>
    <td style="padding:15px">Returns all interfaces of a specific device discovered by running an IPv4
reconciliation policy.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getDiscoveredDeviceInterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoveredDeviceMacAddressEntries(deviceId, policyId, callback)</td>
    <td style="padding:15px">Returns all MAC address entries of a specific device discovered by running an
IPv4 reconciliation p</td>
    <td style="padding:15px">{base_path}/{version}/v1/getDiscoveredDeviceMacAddressEntries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoveredDeviceNetworks(deviceId, policyId, callback)</td>
    <td style="padding:15px">Returns all networks of a specific device discovered by running an IPv4
reconciliation policy.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getDiscoveredDeviceNetworks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoveredDeviceVlans(deviceId, policyId, callback)</td>
    <td style="padding:15px">Returns all VLANs of a specific device discovered by running an IPv4
reconciliation policy.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getDiscoveredDeviceVlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoveredDevices(policyId, callback)</td>
    <td style="padding:15px">Returns a list of discovered Layer 2 or Layer 3 devices by running an IPv4
reconciliation policy sp</td>
    <td style="padding:15px">{base_path}/{version}/v1/getDiscoveredDevices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIP4Address(address, containerId, callback)</td>
    <td style="padding:15px">Returns the details for the requested IPv4 address object.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getIP4Address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIP4NetworksByHint(containerId, count, options, start, callback)</td>
    <td style="padding:15px">Returns an array of IPv4 networks found under a given container object.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getIP4NetworksByHint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIP6Address(address, containerId, callback)</td>
    <td style="padding:15px">Returns an APIEntity for the specified IPv6 address.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getIP6Address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIP6ObjectsByHint(containerId, count, objectType, options, start, callback)</td>
    <td style="padding:15px">Returns an array of IPv6 objects found under a given container object.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getIP6ObjectsByHint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPRangedByIP(address, containerId, type, callback)</td>
    <td style="padding:15px">Returns the DHCP range containing the specified IPv4 or IPv6 address.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getIPRangedByIP?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLinkedNetworkConflicts(networkId, templateId, callback)</td>
    <td style="padding:15px">Get a list of deployment options that conflict with the associated networks
or network that are lin</td>
    <td style="padding:15px">{base_path}/{version}/v1/getLinkedNetworkConflicts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMaxAllowedRange(rangeId, callback)</td>
    <td style="padding:15px">Finds the maximum possible address range to which the existing IPv4 DHCP
range can be extended.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getMaxAllowedRange?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNextAvailableIP4Address(parentId, callback)</td>
    <td style="padding:15px">Returns the IPv4 address for the next available (unallocated) address within
a configuration, block</td>
    <td style="padding:15px">{base_path}/{version}/v1/getNextAvailableIP4Address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNextAvailableIP4Network(autoCreate, isLargerAllowed, parentId, size, callback)</td>
    <td style="padding:15px">Returns the object ID for the next available (unused) network within a
configuration or block.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getNextAvailableIP4Network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNextAvailableIP6Address(parentId, properties, callback)</td>
    <td style="padding:15px">Returns the next available IPv6 address within an IPv6 block or network.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getNextAvailableIP6Address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNextAvailableIPRange(parentId, properties, size, type, callback)</td>
    <td style="padding:15px">Returns the object ID for the next available (unused) block or network within
a configuration or bl</td>
    <td style="padding:15px">{base_path}/{version}/v1/getNextAvailableIPRange?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNextAvailableIPRanges(count, parentId, properties, size, type, callback)</td>
    <td style="padding:15px">Returns the object IDs for the next available (unused) blocks or networks
within a configuration or</td>
    <td style="padding:15px">{base_path}/{version}/v1/getNextAvailableIPRanges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNextIP4Address(parentId, properties, callback)</td>
    <td style="padding:15px">Returns the next available IP addresses in octet notation under specified
circumstances.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getNextIP4Address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplateTaskStatus(taskId, callback)</td>
    <td style="padding:15px">Gets the IPv4 template task status when the template is applied.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getTemplateTaskStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">isAddressAllocated(configurationId, ipAddress, macAddress, callback)</td>
    <td style="padding:15px">Queries a MAC address to determine if the address has been allocated to an IP
address.</td>
    <td style="padding:15px">{base_path}/{version}/v1/isAddressAllocated?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">mergeBlocksWithParent(body, callback)</td>
    <td style="padding:15px">Merges specified IPv4 blocks into a single block.</td>
    <td style="padding:15px">{base_path}/{version}/v1/mergeBlocksWithParent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">mergeSelectedBlocksOrNetworks(blockOrNetworkToKeep, body, callback)</td>
    <td style="padding:15px">Merges specified IPv4 blocks or IPv4 networks into a single IPv4 block or
IPv4 network.</td>
    <td style="padding:15px">{base_path}/{version}/v1/mergeSelectedBlocksOrNetworks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">moveIPObject(address, objectId, options, callback)</td>
    <td style="padding:15px">Moves an IPv4 block, IPv4 network, IPv4 address, IPv6 block, or IPv6 network
to a new IPv4 or IPv6</td>
    <td style="padding:15px">{base_path}/{version}/v1/moveIPObject?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reapplyTemplate(properties, templateId, callback)</td>
    <td style="padding:15px">Reapplies DNS zone templates.</td>
    <td style="padding:15px">{base_path}/{version}/v1/reapplyTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reassignIP6Address(destination, oldAddressId, properties, callback)</td>
    <td style="padding:15px">Reassigns an existing IPv6 address to a new IPv6 address.</td>
    <td style="padding:15px">{base_path}/{version}/v1/reassignIP6Address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeAdditionalIPAddresses(ipsToRemove, properties, serverId, callback)</td>
    <td style="padding:15px">Removes additional IPv4 addresses and loopback addresses from the Services
interface.</td>
    <td style="padding:15px">{base_path}/{version}/v1/removeAdditionalIPAddresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resizeRange(objectId, options, range, callback)</td>
    <td style="padding:15px">Changes the size of an IPv4 block, IPv4 network, DHCPv4 range, IPv6 block, or
IPv6 network.</td>
    <td style="padding:15px">{base_path}/{version}/v1/resizeRange?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">splitIP4Network(networkId, numberOfParts, options, callback)</td>
    <td style="padding:15px">Splits an IPv4 network into the specified number of networks.</td>
    <td style="padding:15px">{base_path}/{version}/v1/splitIP4Network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">splitIP6Range(numberOfParts, options, rangeId, callback)</td>
    <td style="padding:15px">Splits an IPv6 block or network into the specified number of blocks or
networks.</td>
    <td style="padding:15px">{base_path}/{version}/v1/splitIP6Range?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignIP4NetworkTemplate(networkId, properties, templateId, callback)</td>
    <td style="padding:15px">Unassigns IPv4 network templates.</td>
    <td style="padding:15px">{base_path}/{version}/v1/unassignIP4NetworkTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCustomOptionDefinition(allowMultiple, configurationId, name, optionId, optionType, properties, callback)</td>
    <td style="padding:15px">Adds a custom deployment option.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addCustomOptionDefinition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDHCP4Range(end, networkId, properties, start, callback)</td>
    <td style="padding:15px">Adds IPv4 DHCP ranges.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addDHCP4Range?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDHCP4RangeBySize(networkId, offset, properties, size, callback)</td>
    <td style="padding:15px">Adds IPv4 DHCP ranges by offset and percentage.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addDHCP4RangeBySize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDHCP6ClientDeploymentOption(entityId, name, properties, value, callback)</td>
    <td style="padding:15px">Adds DHCPv6 client options and returns the database object ID for the new
option object.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addDHCP6ClientDeploymentOption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDHCP6Range(end, networkId, properties, start, callback)</td>
    <td style="padding:15px">Adds IPv6 DHCP ranges.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addDHCP6Range?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDHCP6RangeBySize(networkId, properties, size, start, callback)</td>
    <td style="padding:15px">Adds IPv6 DHCP ranges by size.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addDHCP6RangeBySize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDHCP6ServiceDeploymentOption(entityId, name, properties, value, callback)</td>
    <td style="padding:15px">Adds DHCPv6 service options.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addDHCP6ServiceDeploymentOption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDHCPClientDeploymentOption(entityId, name, properties, value, callback)</td>
    <td style="padding:15px">Adds a DHCP client option and returns the object ID for the new option object.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addDHCPClientDeploymentOption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDHCPMatchClass(configurationId, matchCriteria, name, properties, callback)</td>
    <td style="padding:15px">Adds DHCP match classes to Address Manager.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addDHCPMatchClass?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDHCPServiceDeploymentOption(entityId, name, properties, value, callback)</td>
    <td style="padding:15px">Adds DHCP service options.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addDHCPServiceDeploymentOption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDHCPSubClass(matchClassId, matchValue, properties, callback)</td>
    <td style="padding:15px">Adds DHCP match class values.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addDHCPSubClass?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDHCPVendorDeploymentOption(optionId, parentId, properties, value, callback)</td>
    <td style="padding:15px">Adds a DHCP vendor deployment option to specified objects.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addDHCPVendorDeploymentOption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addVendorOptionDefinition(allowMultiple, description, name, optionId, optionType, properties, vendorProfileId, callback)</td>
    <td style="padding:15px">Adds a vendor option definition to a vendor profile.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addVendorOptionDefinition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addVendorProfile(description, identifier, name, properties, callback)</td>
    <td style="padding:15px">Adds a vendor profile and returns the object ID for the new vendor profile.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addVendorProfile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDHCP6ClientDeploymentOption(entityId, name, serverId, callback)</td>
    <td style="padding:15px">Deletes DHCPv6 client options.</td>
    <td style="padding:15px">{base_path}/{version}/v1/deleteDHCP6ClientDeploymentOption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDHCP6ServiceDeploymentOption(entityId, name, serverId, callback)</td>
    <td style="padding:15px">Deletes DHCPv6 service options.</td>
    <td style="padding:15px">{base_path}/{version}/v1/deleteDHCP6ServiceDeploymentOption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDHCPClientDeploymentOption(entityId, name, serverId, callback)</td>
    <td style="padding:15px">Deletes DHCP client options.</td>
    <td style="padding:15px">{base_path}/{version}/v1/deleteDHCPClientDeploymentOption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDHCPServiceDeploymentOption(entityId, name, serverId, callback)</td>
    <td style="padding:15px">Deletes DHCP service options.</td>
    <td style="padding:15px">{base_path}/{version}/v1/deleteDHCPServiceDeploymentOption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDHCPVendorDeploymentOption(entityId, optionId, serverId, callback)</td>
    <td style="padding:15px">Deletes a specified DHCP vendor deployment option.</td>
    <td style="padding:15px">{base_path}/{version}/v1/deleteDHCPVendorDeploymentOption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCP6ClientDeploymentOption(entityId, name, serverId, callback)</td>
    <td style="padding:15px">Returns DHCPv6 client options assigned for the object specified excluding the
options inherited fro</td>
    <td style="padding:15px">{base_path}/{version}/v1/getDHCP6ClientDeploymentOption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCP6ServiceDeploymentOption(entityId, name, serverId, callback)</td>
    <td style="padding:15px">Returns DHCPv6 service options assigned for the object specified excluding
the options inherited fr</td>
    <td style="padding:15px">{base_path}/{version}/v1/getDHCP6ServiceDeploymentOption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPClientDeploymentOption(entityId, name, serverId, callback)</td>
    <td style="padding:15px">Returns DHCPv4 client options assigned for the object specified excluding the
options inherited fro</td>
    <td style="padding:15px">{base_path}/{version}/v1/getDHCPClientDeploymentOption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPServiceDeploymentOption(entityId, name, serverId, callback)</td>
    <td style="padding:15px">Returns DHCP service options assigned for the object specified excluding the
options inherited from</td>
    <td style="padding:15px">{base_path}/{version}/v1/getDHCPServiceDeploymentOption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPVendorDeploymentOption(entityId, optionId, serverId, callback)</td>
    <td style="padding:15px">Retrieves a DHCP vendor deployment option assigned for the object specified
excluding the options i</td>
    <td style="padding:15px">{base_path}/{version}/v1/getDHCPVendorDeploymentOption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSharedNetworks(tagId, callback)</td>
    <td style="padding:15px">Returns multiple IPv4 networks linked to the given shared network tag.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getSharedNetworks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">shareNetwork(networkId, tagId, callback)</td>
    <td style="padding:15px">Links an IPv4 network with a shared network tag.</td>
    <td style="padding:15px">{base_path}/{version}/v1/shareNetwork?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unshareNetwork(networkId, callback)</td>
    <td style="padding:15px">Unlinks the shared network tag from an IPv4 network.</td>
    <td style="padding:15px">{base_path}/{version}/v1/unshareNetwork?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDHCP6ClientDeploymentOption(body, callback)</td>
    <td style="padding:15px">Updates DHCPv6 client options.</td>
    <td style="padding:15px">{base_path}/{version}/v1/updateDHCP6ClientDeploymentOption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDHCP6ServiceDeploymentOption(body, callback)</td>
    <td style="padding:15px">Updates DHCPv6 service options.</td>
    <td style="padding:15px">{base_path}/{version}/v1/updateDHCP6ServiceDeploymentOption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDHCPClientDeploymentOption(body, callback)</td>
    <td style="padding:15px">Updates DHCP client options.</td>
    <td style="padding:15px">{base_path}/{version}/v1/updateDHCPClientDeploymentOption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDHCPServiceDeploymentOption(body, callback)</td>
    <td style="padding:15px">Updates DHCP service options.</td>
    <td style="padding:15px">{base_path}/{version}/v1/updateDHCPServiceDeploymentOption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDHCPVendorDeploymentOption(body, callback)</td>
    <td style="padding:15px">Updates the specified DHCP vendor deployment option.</td>
    <td style="padding:15px">{base_path}/{version}/v1/updateDHCPVendorDeploymentOption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDHCPDeploymentRole(entityId, properties, serverInterfaceId, type, callback)</td>
    <td style="padding:15px">Adds a DHCP deployment role to a specified object.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addDHCPDeploymentRole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDNSDeploymentRole(entityId, properties, serverInterfaceId, type, callback)</td>
    <td style="padding:15px">Adds a DNS deployment role to a specified object.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addDNSDeploymentRole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addServer(configurationId, defaultInterfaceAddress, fullHostName, name, profile, properties, callback)</td>
    <td style="padding:15px">Adds servers to Address Manager.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addServer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTFTPDeploymentRole(entityId, properties, serverId, callback)</td>
    <td style="padding:15px">Adds a TFTP deployment role to a specified object.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addTFTPDeploymentRole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDHCPDeploymentRole(entityId, serverInterfaceId, callback)</td>
    <td style="padding:15px">Deletes DHCP deployment roles.</td>
    <td style="padding:15px">{base_path}/{version}/v1/deleteDHCPDeploymentRole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSDeploymentRole(entityId, serverInterfaceId, callback)</td>
    <td style="padding:15px">Deletes a specified DNS deployment role.</td>
    <td style="padding:15px">{base_path}/{version}/v1/deleteDNSDeploymentRole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSDeploymentRoleForView(entityId, serverInterfaceId, viewId, callback)</td>
    <td style="padding:15px">Deletes the DNS deployment role assigned to view-level objects in the IP
space for ARPA zones.</td>
    <td style="padding:15px">{base_path}/{version}/v1/deleteDNSDeploymentRoleForView?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployServer(serverId, callback)</td>
    <td style="padding:15px">Deploys servers.</td>
    <td style="padding:15px">{base_path}/{version}/v1/deployServer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployServerConfig(properties, serverId, callback)</td>
    <td style="padding:15px">Deploys specific configuration(s) to a particular server.</td>
    <td style="padding:15px">{base_path}/{version}/v1/deployServerConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployServerServices(serverId, services, callback)</td>
    <td style="padding:15px">Deploys specific service(s) to a particular server.</td>
    <td style="padding:15px">{base_path}/{version}/v1/deployServerServices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDHCPDeploymentRole(entityId, serverInterfaceId, callback)</td>
    <td style="padding:15px">Retrieves the DHCP deployment role assigned to a specified object.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getDHCPDeploymentRole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSDeploymentRole(entityId, serverInterfaceId, callback)</td>
    <td style="padding:15px">Retrieves a DNS deployment role from a specified object.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getDNSDeploymentRole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSDeploymentRoleForView(entityId, serverInterfaceId, viewId, callback)</td>
    <td style="padding:15px">Retrieves the DNS deployment role assigned to a view-level objects in the IP
space for ARPA zones.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getDNSDeploymentRoleForView?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeploymentRoles(entityId, callback)</td>
    <td style="padding:15px">Returns the DNS and DHCP deployment roles associated with the specified
object.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getDeploymentRoles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeploymentTaskStatus(deploymentTaskToken, callback)</td>
    <td style="padding:15px">Returns the deployment status of the deployment task that was created using
the.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getDeploymentTaskStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServerDeploymentRoles(serverId, callback)</td>
    <td style="padding:15px">Returns a list of all deployment roles associated with the server.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getServerDeploymentRoles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServerDeploymentStatus(properties, serverId, callback)</td>
    <td style="padding:15px">Returns the deployment status of the server.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getServerDeploymentStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServerForRole(roleId, callback)</td>
    <td style="padding:15px">Returns a list of all servers associated with the specified deployment role.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getServerForRole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">moveDeploymentRoles(moveDhcpRoles, moveDnsRoles, options, sourceServerId, targetServerInterfaceId, callback)</td>
    <td style="padding:15px">Moves all DNS and DHCP deployment roles from a server to the specified
interface of another server.</td>
    <td style="padding:15px">{base_path}/{version}/v1/moveDeploymentRoles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">quickDeploy(entityId, properties, callback)</td>
    <td style="padding:15px">Instantly deploys changes you made to DNS resource records
since the last full or quick deployment.</td>
    <td style="padding:15px">{base_path}/{version}/v1/quickDeploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceServer(defaultInterface, hostName, name, password, properties, serverId, upgrade, callback)</td>
    <td style="padding:15px">Replaces a server.</td>
    <td style="padding:15px">{base_path}/{version}/v1/replaceServer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">selectiveDeploy(properties, body, callback)</td>
    <td style="padding:15px">Selectively deploys—creates a differential deployment task to deploy changes made to specific DNS
e</td>
    <td style="padding:15px">{base_path}/{version}/v1/selectiveDeploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDHCPDeploymentRole(body, callback)</td>
    <td style="padding:15px">Updates a DHCP deployment role.</td>
    <td style="padding:15px">{base_path}/{version}/v1/updateDHCPDeploymentRole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDNSDeploymentRole(body, callback)</td>
    <td style="padding:15px">Updates a specified DNS deployment role.</td>
    <td style="padding:15px">{base_path}/{version}/v1/updateDNSDeploymentRole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addEntity(parentId, body, callback)</td>
    <td style="padding:15px">A generic method for adding configurations, DNS zones, and DNS resource
records.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addEntity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignOrUpdateTemplate(entityId, properties, templateId, callback)</td>
    <td style="padding:15px">Assigns, updates, or removes DNS zone and IPv4 network templates.</td>
    <td style="padding:15px">{base_path}/{version}/v1/assignOrUpdateTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">customSearch(count, filters, options, start, type, callback)</td>
    <td style="padding:15px">Search for an array of entities by specifying object properties.</td>
    <td style="padding:15px">{base_path}/{version}/v1/customSearch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete(objectId, callback)</td>
    <td style="padding:15px">Deletes an object using the generic.</td>
    <td style="padding:15px">{base_path}/{version}/v1/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWithOptions(objectId, options, callback)</td>
    <td style="padding:15px">Deletes objects that have options associated with their removal.</td>
    <td style="padding:15px">{base_path}/{version}/v1/deleteWithOptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEntities(count, parentId, start, type, callback)</td>
    <td style="padding:15px">Returns multiple entities for the specified parent ID.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getEntities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEntitiesByName(count, name, parentId, start, type, callback)</td>
    <td style="padding:15px">Returns an array of entities that match the specified parent, name, and
object type.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getEntitiesByName?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEntitiesByNameUsingOptions(count, name, options, parentId, start, type, callback)</td>
    <td style="padding:15px">Returns an array of entities that match the specified name and object type.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getEntitiesByNameUsingOptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEntityByCIDR(cidr, parentId, type, callback)</td>
    <td style="padding:15px">Returns an IPv4 Network object from the database by calling it using CIDR
notation.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getEntityByCIDR?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEntityById(id, callback)</td>
    <td style="padding:15px">Returns objects from the database referenced by their database ID and with
its properties fields po</td>
    <td style="padding:15px">{base_path}/{version}/v1/getEntityById?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEntityByName(name, parentId, type, callback)</td>
    <td style="padding:15px">Returns objects from the database referenced by their name field.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getEntityByName?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEntityByPrefix(containerId, prefix, type, callback)</td>
    <td style="padding:15px">Returns an APIEntity for the specified IP block or network.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getEntityByPrefix?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEntityByRange(address1, address2, parentId, type, callback)</td>
    <td style="padding:15px">Returns an IPv4 DHCP range object by calling it using its range.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getEntityByRange?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLinkedEntities(count, entityId, start, type, callback)</td>
    <td style="padding:15px">Returns an array of entities containing the entities linked to a specified
entity.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getLinkedEntities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getParent(entityId, callback)</td>
    <td style="padding:15px">Returns the parent entity of a given entity.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getParent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemInfo(callback)</td>
    <td style="padding:15px">Gets Address Manager system information.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getSystemInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">linkEntities(entity1Id, entity2Id, properties, callback)</td>
    <td style="padding:15px">Establishes a link between two specified Address Manager entities.</td>
    <td style="padding:15px">{base_path}/{version}/v1/linkEntities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">login(password, username, callback)</td>
    <td style="padding:15px">Logs in as API user.</td>
    <td style="padding:15px">{base_path}/{version}/v1/login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loginWithOptions(options, password, username, callback)</td>
    <td style="padding:15px">Logs in as an API user with the option to change the locale to Japanese or Simplified Chinese.</td>
    <td style="padding:15px">{base_path}/{version}/v1/loginWithOptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logout(callback)</td>
    <td style="padding:15px">Logs out of the current API session.</td>
    <td style="padding:15px">{base_path}/{version}/v1/logout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchByCategory(category, count, keyword, start, callback)</td>
    <td style="padding:15px">Returns an array of entities by searching for keywords associated with
objects of a specified objec</td>
    <td style="padding:15px">{base_path}/{version}/v1/searchByCategory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchByObjectTypes(count, keyword, start, types, callback)</td>
    <td style="padding:15px">Returns an array of entities by searching for keywords associated with
objects of a specified objec</td>
    <td style="padding:15px">{base_path}/{version}/v1/searchByObjectTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unlinkEntities(entity1Id, entity2Id, properties, callback)</td>
    <td style="padding:15px">Removes the link between two specified Address Manager entities.</td>
    <td style="padding:15px">{base_path}/{version}/v1/unlinkEntities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update(body, callback)</td>
    <td style="padding:15px">Updates entity objects.</td>
    <td style="padding:15px">{base_path}/{version}/v1/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateWithOptions(options, body, callback)</td>
    <td style="padding:15px">Updates objects requiring a certain behavior that is not covered by the
regular.</td>
    <td style="padding:15px">{base_path}/{version}/v1/updateWithOptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addRawDeploymentOption(parentId, body, callback)</td>
    <td style="padding:15px">Adds raw deployment options.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addRawDeploymentOption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeploymentOptions(entityId, optionTypes, serverId, callback)</td>
    <td style="padding:15px">Retrieves deployment options for Address Manager DNS and DHCP services.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getDeploymentOptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRawDeploymentOption(body, callback)</td>
    <td style="padding:15px">Updates raw deployment options.</td>
    <td style="padding:15px">{base_path}/{version}/v1/updateRawDeploymentOption?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTFTPFile(inputStream, name, parentId, properties, version, body, callback)</td>
    <td style="padding:15px">Adds TFTP files.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addTFTPFile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTFTPFolder(name, parentId, properties, callback)</td>
    <td style="padding:15px">Adds TFTP folders.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addTFTPFolder?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTFTPGroup(configurationId, name, properties, callback)</td>
    <td style="padding:15px">Adds TFTP groups.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addTFTPGroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addUserDefinedField(type, body, callback)</td>
    <td style="padding:15px">Adds user-defined fields.</td>
    <td style="padding:15px">{base_path}/{version}/v1/addUserDefinedField?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUserDefinedField(name, type, callback)</td>
    <td style="padding:15px">Deletes user-defined fields.</td>
    <td style="padding:15px">{base_path}/{version}/v1/deleteUserDefinedField?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserDefinedFields(requiredFieldsOnly, type, callback)</td>
    <td style="padding:15px">Returns the user-defined fields information.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getUserDefinedFields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBulkUdf(inputStream, properties, body, callback)</td>
    <td style="padding:15px">Updates values of various user-defined fields (UDFs) for different objects.</td>
    <td style="padding:15px">{base_path}/{version}/v1/updateBulkUdf?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUserDefinedField(type, body, callback)</td>
    <td style="padding:15px">Updates user-defined fields.</td>
    <td style="padding:15px">{base_path}/{version}/v1/updateUserDefinedField?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">breakXHAPair(breakInProteusOnly, xHAServerId, callback)</td>
    <td style="padding:15px">Breaks an xHA pair and returns each server to its original stand-alone state.</td>
    <td style="padding:15px">{base_path}/{version}/v1/breakXHAPair?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createXHAPair(activeServerId, activeServerNewIPv4Address, configurationId, passiveServerId, properties, callback)</td>
    <td style="padding:15px">Creates an xHA pair.</td>
    <td style="padding:15px">{base_path}/{version}/v1/createXHAPair?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editXHAPair(name, properties, xHAServerId, callback)</td>
    <td style="padding:15px">Updates the xHA pair created.</td>
    <td style="padding:15px">{base_path}/{version}/v1/editXHAPair?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">failoverXHA(xHAServerId, callback)</td>
    <td style="padding:15px">Performs a manual xHA failover.</td>
    <td style="padding:15px">{base_path}/{version}/v1/failoverXHA?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cleanBrokenTrustRelationship(ipsToRemove, callback)</td>
    <td style="padding:15px">This is an internal API used by Address Manager services.</td>
    <td style="padding:15px">{base_path}/{version}/v1/cleanBrokenTrustRelationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configureReplication(compressReplication, properties, replicationBreakThreshold, replicationQueueThreshold, standbyServer, callback)</td>
    <td style="padding:15px">This API has been deprecated and is no longer supported.</td>
    <td style="padding:15px">{base_path}/{version}/v1/configureReplication?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exchangeKeyBundles(body, callback)</td>
    <td style="padding:15px">This is an internal API used by Address Manager services.</td>
    <td style="padding:15px">{base_path}/{version}/v1/exchangeKeyBundles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">moveIP4Object(address, objectId, callback)</td>
    <td style="padding:15px">This API has been deprecated and is no longer supported.</td>
    <td style="padding:15px">{base_path}/{version}/v1/moveIP4Object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProbeData(definedProbe, properties, callback)</td>
    <td style="padding:15px">Returns the JSON response from the properties field of the APIData object.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getProbeData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProbeStatus(definedProbe, callback)</td>
    <td style="padding:15px">Returns the status of the triggered data collection process.</td>
    <td style="padding:15px">{base_path}/{version}/v1/getProbeStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startProbe(definedProbe, properties, callback)</td>
    <td style="padding:15px">Starts collecting data from the Address Manager database using pre-defined SQL
queries.</td>
    <td style="padding:15px">{base_path}/{version}/v1/startProbe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">isMigrationRunning(filename, callback)</td>
    <td style="padding:15px">Returns true or false to indicate if the migration service is running.</td>
    <td style="padding:15px">{base_path}/{version}/v1/isMigrationRunning?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrateFile(filename, callback)</td>
    <td style="padding:15px">Migrates the specified XML file into Address Manager.</td>
    <td style="padding:15px">{base_path}/{version}/v1/migrateFile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
